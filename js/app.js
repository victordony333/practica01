function cargarAjax(){

    const url = "https://jsonplaceholder.typicode.com/todos"

    axios
    .get(url)
    .then((res)=>{
        mostrar(res.data);
    })
    .catch((err)=>{
        console.log("Error");
    })
    function mostrar(data){
        const res = document.getElementById('respuesta');
        res.innerHTML="";

        for(item of data){

            res.innerHTML+= "<hr><br>" + "UserID: " + item.userId + "<br>" + "ID: " + item.id 
            + "<br>" + "Title: " + item.title + "<br>" 
            + "Completed: " + item.completed + "<br><br><hr>" 

        }

    }

}

const res = document.getElementById('btnCargar');

res.addEventListener('click', function(){
    cargarAjax();
})

document.getElementById('btnLimpiar')
.addEventListener('click',function(){

    const res = document.getElementById('respuesta');

    res.innerHTML="";
});